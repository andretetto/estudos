package capitulo5;

import capitulo2.Usuario;

import java.util.ArrayList;
import java.util.List;

import static java.util.Comparator.comparing;
import static java.util.Comparator.comparingInt;

public class capitulo5 {
    public static void main (String ... args){

        Usuario usuario1 = new Usuario(200, "Pedro");
        Usuario usuario2 = new Usuario(150, "André");
        Usuario usuario3 = new Usuario(180, "Lucas");

        List<Usuario> usuarios = new ArrayList<>();
        usuarios.add(usuario1);
        usuarios.add(usuario2);
        usuarios.add(usuario3);

//        Comparator<Usuario> comparator = new Comparator<Usuario>() {
//            @Override
//            public int compare(Usuario u1, Usuario u2) {
//                return u1.getNome().compareToIgnoreCase(u2.getNome());
//            }
//        };

//        Collections.sort(usuarios,
//                (u1, u2) -> u1.getNome().compareToIgnoreCase(u2.getNome()));

//        Collections.sort(usuarios,
//                (u1, u2) -> String.CASE_INSENSITIVE_ORDER.compare(u1.getNome(), u2.getNome()));

//        usuarios.sort((u1, u2) -> String.CASE_INSENSITIVE_ORDER.compare(u1.getNome(), u2.getNome()));

        //usuarios.sort(comparing(usuario -> usuario.getNome()));
        //usuarios.sort(comparing(usuario -> usuario.getPontos()));
        usuarios.sort(comparingInt(usuario -> usuario.getPontos()));
        usuarios.forEach(usuario -> System.out.println(usuario.getNome()));




    }

}
