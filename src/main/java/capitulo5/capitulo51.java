package capitulo5;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class capitulo51{
    public static void main (String ... args){
        List<String> palavras = Arrays.asList("Pedro", "André", "Lucas");
        //Collections.sort(palavras);
        //palavras.sort(Comparator.naturalOrder());
        palavras.sort(Comparator.reverseOrder());
        palavras.forEach(s -> System.out.println(s));

    }
}
