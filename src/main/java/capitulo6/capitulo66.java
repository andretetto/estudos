package capitulo6;

import capitulo2.Usuario;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

public class capitulo66 {
    public static void main (String ... args){
//        Fornecedor fornecedor = new Fornecedor();
//        Usuario andre = fornecedor.get();

//        Supplier<Usuario> usuarioSupplier = new Supplier<Usuario>() {
//            @Override
//            public Usuario get() {
//                return new Usuario();
//            }
//        };
//        Usuario andre = usuarioSupplier.get();

//        Supplier<Usuario> usuarioSupplier = () -> new Usuario();
//        Usuario andre = usuarioSupplier.get();

        Supplier<Usuario> criadorDeUsuarios = Usuario::new;
        Usuario andre = criadorDeUsuarios.get();

        Function<String, Usuario> criadorDeUsuarios1 = Usuario::new;
        Usuario pedro = criadorDeUsuarios1.apply("Pedro");
        System.out.println(pedro);

        Usuario lucas = criadorDeUsuarios1.apply("Lucas");
        System.out.println(lucas);

        //BiFunction<Integer, String, Usuario> criadorDeUsuarios2 = (integer, s) -> new Usuario(integer,s);
        BiFunction<Integer, String, Usuario> criadorDeUsuarios2 = Usuario::new;
        Usuario camila = criadorDeUsuarios2.apply(300, "Camila");
        System.out.println(camila);

        //TriFunction<Boolean, Integer, String, Usuario> criadorDeUsuarios3 = (aBoolean, integer, s) -> new Usuario(aBoolean,integer,s);
        TriFunction<Boolean, Integer, String, Usuario> criadorDeUsuarios3 = Usuario::new;
        Usuario joao = criadorDeUsuarios3.apply(true, 200, "João");
        System.out.println(joao);

    }
}
