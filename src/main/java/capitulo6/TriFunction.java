package capitulo6;

@FunctionalInterface
public interface TriFunction<T, U, R, V> {
    V apply(T t, U u, R r);
}
