package capitulo6;

import capitulo2.Usuario;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.ToIntFunction;

import static java.util.Comparator.*;

public class capitulo6 {
    public static void main (String ... args){
        Usuario usuario0 = null;
        Usuario usuario1 = new Usuario(150, "Pedro");
        Usuario usuario2 = new Usuario(200, "Lucas");
        Usuario usuario3 = new Usuario(150, "André");

        List<Usuario> usuarios = new ArrayList<>();
        usuarios.add(usuario0);
        usuarios.add(usuario1);
        usuarios.add(usuario2);
        usuarios.add(usuario3);

        //usuarios.forEach(usuario -> usuario.tornaModerador());
        //usuarios.sort(comparing(usuario -> usuario.getNome()));
        //usuarios.sort(comparing(Usuario::getNome));
        Consumer<Usuario> imprimiNome = usuario -> {
            if (usuario != null){
                System.out.println(usuario.getNome());
            }
        };

        Function<Usuario, String> byName = Usuario::getNome;
        usuarios.sort(nullsLast(comparing(byName)));
        usuarios.forEach(imprimiNome);
        System.out.println("-----Fim-----");

        ToIntFunction<Usuario> byPontos = Usuario::getPontos;
        usuarios.sort(nullsFirst(comparingInt(byPontos).reversed()));
        usuarios.forEach(imprimiNome);
        System.out.println("-----Fim-----");

        usuarios.sort(nullsLast(comparingInt(byPontos).thenComparing(byName)));
        usuarios.forEach(imprimiNome);
        System.out.println("-----Fim-----");

        usuarios.sort(nullsFirst(comparingInt(byPontos).reversed().thenComparing(byName)));
        usuarios.forEach(imprimiNome);
        System.out.println("-----Fim-----");
    }
}
