package capitulo6;

import capitulo2.Usuario;

import java.util.function.Consumer;

public class capitulo61Treino {
    public static void main1 (String ... args){
        Usuario usuario0 = new Usuario(200, "André");
        //Consumer<Usuario> consumer = usuario -> usuario.tornaModerador();
        //Consumer<Usuario> consumer = usuario -> System.out.println(usuario);
        //Consumer<Usuario> consumer = Usuario::tornaModerador;
        Consumer<Usuario> consumer = System.out::println;
        consumer.accept(usuario0);
    }

    public static void main (String ... args){
        Usuario usuario0 = new Usuario(200, "André");
        //Runnable runnable = () -> System.out.println(usuario0);
        //Runnable runnable = () -> usuario0.tornaModerador();
        //Runnable runnable = usuario0::tornaModerador;
        Runnable runnable = System.out::println;
        runnable.run();
    }
}
