package capitulo6;

import capitulo2.Usuario;

import java.util.function.Consumer;

public class capitulo61 {
    public static void main (String ... args){
        Usuario usuario = new Usuario(100, "André");
        //Runnable runnable = () -> System.out.println(usuario);
        //Runnable runnable = () -> usuario.tornaModerador();
        Runnable runnable = usuario::tornaModerador;
        runnable.run();

        Usuario usuario1 = new Usuario(150, "Pedro");
        //Consumer<Usuario> consumer = usuario2 -> System.out.println(usuario2);
        //Consumer<Usuario> consumer = usuario2 -> usuario2.tornaModerador();
        //Consumer<Usuario> consumer = Usuario::tornaModerador;
        Consumer<Usuario> consumer = System.out::println;
        consumer.accept(usuario1);

    }
}
