package capitulo6;

import java.util.function.BiFunction;
import java.util.function.IntBinaryOperator;
import java.util.function.ToIntBiFunction;

public class capitulo67 {
    public static void main(String ... args){
        //BiFunction<Integer, Integer, Integer> numeroMaximo = (integer, integer2) -> Math.max(integer, integer2);
        //BiFunction<Integer, Integer, Integer> numeroMaximo = Math::max;
        //System.out.println(numeroMaximo.apply(5,4));
        ToIntBiFunction<Integer, Integer> numeroMaximo = Math::max;
        System.out.println(numeroMaximo.applyAsInt(5,4));

        IntBinaryOperator numeroMaximo2 = Math::max;
        System.out.println(numeroMaximo2.applyAsInt(5,4));
    }
}
