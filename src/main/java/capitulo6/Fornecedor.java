package capitulo6;

import capitulo2.Usuario;

import java.util.function.Supplier;

public class Fornecedor implements Supplier<Usuario> {
    @Override
    public Usuario get() {
        return new Usuario();
    }
}
