package capitulo6;

import capitulo2.Usuario;

import java.util.Arrays;
import java.util.List;

public class capitulo65 {
    public static void main (String ... args){
        Usuario usuario0 = new Usuario(150, "Lucas");
        Usuario usuario1 = new Usuario(200, "Pedro");
        Usuario usuario2 = new Usuario(100, "André");

        List<Usuario> usuarios = Arrays.asList(usuario0,usuario1,usuario2);

        //usuarios.forEach(usuario -> System.out.println(usuario));
        usuarios.forEach(System.out::println);

    }
}
