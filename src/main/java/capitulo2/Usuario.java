package capitulo2;

public class Usuario {
    private int pontos;
    private String nome;
    private boolean moderador;

    public Usuario() {}

    public Usuario(String nome) {
        this.nome = nome;
    }

    public Usuario(int pontos, String nome) {
        this.pontos = pontos;
        this.nome = nome;
        this.moderador = false;
    }

    public Usuario( boolean moderador, int pontos, String nome) {
        this(pontos, nome);
        this.moderador = moderador;
    }

    public int getPontos() {
        return pontos;
    }

    public String getNome() {
        return nome;
    }

    public boolean isModerador() {
        return moderador;
    }

    public void tornaModerador(){
        this.moderador = true;
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "pontos=" + pontos +
                ", nome='" + nome + '\'' +
                ", moderador=" + moderador +
                '}';
    }

}
