package capitulo2;

import java.util.Arrays;
import java.util.List;

public class Capitulo2 {
    public static void main(String args[])
    {
        Usuario usuario1 = new Usuario(100, "André");
        Usuario usuario2 = new Usuario(150, "Pedro");
        Usuario usuario3 = new Usuario(180, "Lucas");

        List<Usuario> usuarios = Arrays.asList( usuario1, usuario2, usuario3);
        //capitulo2.Mostrador mostrador = new capitulo2.Mostrador();
//        Consumer<capitulo2.Usuario> mostrador = new Consumer<capitulo2.Usuario>() {
//            @Override
//            public void accept(capitulo2.Usuario usuario) {
//                System.out.println(usuario.getNome());
//            }
//        };
//        usuarios.forEach(mostrador);
        usuarios.forEach(usuario -> System.out.println(usuario.getNome()));
    }
}
