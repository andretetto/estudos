package capitulo10;

import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Locale;

public class capitulo10 {
    public static void main (String ... args){
        Calendar mesQueVem = Calendar.getInstance();
        mesQueVem.add(Calendar.MONTH, 1);
        System.out.println(mesQueVem);

        LocalDate mesQueVem1 = LocalDate.now().plusMonths(1);
        System.out.println(mesQueVem1);

        LocalDate anoPassado = LocalDate.now().minusYears(1);
        System.out.println(anoPassado);

        LocalDateTime agora = LocalDateTime.now();
        System.out.println(agora);

        LocalTime agora1 = LocalTime.now();
        System.out.println(agora1);

        LocalDateTime hojeAoMeioDia = LocalDate.now().atTime(12,0);
        System.out.println(hojeAoMeioDia);

        LocalTime agora2 = LocalTime.now();
        LocalDate hoje =  LocalDate.now();
        LocalDateTime dataEHora = hoje.atTime(agora2);
        System.out.println(dataEHora);

        ZonedDateTime dataHoraTimezone = dataEHora.atZone(ZoneId.of("America/Sao_Paulo"));
        System.out.println(dataHoraTimezone);

        LocalDateTime semTimezone = dataHoraTimezone.toLocalDateTime();
        System.out.println(semTimezone);
        System.out.println(semTimezone.toString());

        LocalDate date = LocalDate.of(2019,05,25);
        System.out.println(date);

        LocalDateTime dateTime = LocalDateTime.of(2019,05, 25, 9, 00, 05);
        System.out.println(dateTime);

        LocalDate datadoPassado = LocalDate.now().withYear(1988).withMonth(10).withDayOfMonth(6);
        System.out.println(datadoPassado);

        System.out.println(datadoPassado.getDayOfMonth());
        System.out.println(datadoPassado.getMonth());
        System.out.println(datadoPassado.getYear());

        LocalDate hoje2 = LocalDate.now();
        LocalDate amanha = LocalDate.now().plusDays(1);
        System.out.println(hoje2.isBefore(amanha));
        System.out.println(hoje2.isAfter(amanha));
        System.out.println(hoje2.isEqual(amanha));

        ZonedDateTime tokyo = ZonedDateTime.of(2011, 5,2, 10, 30, 0,0, ZoneId.of("Asia/Tokyo"));
        ZonedDateTime saoPaulo = ZonedDateTime.of(2011, 5, 2, 10, 30, 0, 0 , ZoneId.of("America/Sao_Paulo"));
        tokyo = tokyo.plusHours(12);
        System.out.println(tokyo.isEqual(saoPaulo));

        System.out.println("hoje é dia: " + MonthDay.now().getDayOfMonth());

        YearMonth ym = YearMonth.from(LocalDate.now());
        System.out.println(ym.getMonth() + " " + ym.getYear());

        System.out.println(YearMonth.now());

        System.out.println(LocalDate.now().of(2019,12,25));
        System.out.println(LocalDate.now().of(2019,Month.DECEMBER,25));

        System.out.println(Month.DECEMBER.firstMonthOfQuarter());
        System.out.println(Month.DECEMBER.plus(2));
        System.out.println(Month.DECEMBER.minus(1));
        System.out.println(Month.DECEMBER.getValue());
        System.out.println(Month.DECEMBER.maxLength());
        System.out.println(Month.DECEMBER.minLength());

        Locale pt = new Locale("pt");
        //Locale pt = Locale.US;
        System.out.println(Month.DECEMBER.getDisplayName(TextStyle.FULL,pt));
        System.out.println(Month.DECEMBER.getDisplayName(TextStyle.SHORT,pt));
        System.out.println(Month.DECEMBER.getDisplayName(TextStyle.NARROW,pt));


        LocalDateTime agora3 = LocalDateTime.now();
        String resultado = agora3.format(DateTimeFormatter.ISO_LOCAL_TIME);
        System.out.println(agora3);

        String resultado1 = LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        System.out.println(resultado1);

        String resultado2 = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm:ss", pt));
        System.out.println(resultado2);

        DateTimeFormatter formatador = DateTimeFormatter.ofPattern("dd/MM/yyyy", pt);
        String resultado3 = LocalDateTime.now().format(formatador);
        System.out.println(resultado3);
        LocalDate resultado4 = LocalDate.parse(resultado3, formatador);
        System.out.println(resultado4.format(formatador));


        Calendar instance = Calendar.getInstance();
        instance.set(2014, Calendar.FEBRUARY, 30);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");
        System.out.println(dateFormat.format(instance.getTime()));

        //LocalDate.of(2014, Month.FEBRUARY, 30);
        //LocalDateTime horaInvalida = LocalDate.now().atTime(25,0);


        Calendar agora4 = Calendar.getInstance();
        Calendar outraData = Calendar.getInstance();
        outraData.set(1988, Calendar.JANUARY,25);

        Long diferenca = agora4.getTimeInMillis() - outraData.getTimeInMillis();
        long milissegundosDeumDia = 1000 * 60 * 60 * 24;
        long dias = diferenca/milissegundosDeumDia;
        System.out.println(dias);


        LocalDate agora5 = LocalDate.now();
        LocalDate outraData1 = LocalDate.of(1988,Month.JANUARY,25);
        long dias1 = ChronoUnit.DAYS.between(outraData1, agora5);
        long meses = ChronoUnit.MONTHS.between(outraData1, agora5);
        long anos = ChronoUnit.YEARS.between(outraData1, agora5);
        System.out.printf("%s dias, %s meses e %s anos", dias1, meses, anos);
        System.out.println("");

        LocalDate agora6 = LocalDate.now();
        LocalDate antes = LocalDate.of(1988, Month.JANUARY, 25);
        Period periodo = Period.between(antes, agora6);
        System.out.printf("%s dias, %s meses e %s anos", periodo.getDays(), periodo.getMonths(), periodo.getYears());
        System.out.println("");

        LocalDate agora7 = LocalDate.now();
        LocalDate futuro = LocalDate.of(2020, Month.JANUARY, 25);
        Period periodo1 = Period.between(futuro, agora7);
        if (periodo1.isNegative()){
            periodo1 = periodo1.negated();
        }
        System.out.printf("%s dias, %s meses e %s anos", periodo1.getDays(), periodo1.getMonths(), periodo1.getYears());
        System.out.println("");

        LocalDateTime agora8 = LocalDateTime.now();
        LocalDateTime daquiUmaHora = LocalDateTime.now().plusHours(1);
        //Duration duration = Duration.between(agora, daquiUmaHora);
        Duration duration = Duration.between(daquiUmaHora, agora);
        if (duration.isNegative()){
            duration = duration.negated();
        }
        System.out.printf("%s horas, %s minutos e %s segundos", duration.toHours(), duration.toMinutes(), duration.getSeconds());
        System.out.println("");

       /* String dataString = "2019-04-25";

        LocalDate date1 = LocalDate.parse(dataString);
        System.out.println(date1);
*/
    }
}
