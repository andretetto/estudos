package capitulo8;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class capitulo88a {
    public static void main (String ... args) throws IOException {
//        Files.list(Paths.get("/home/22341970826/workspace-intelliJ/estudos/src/main/java/capitulo8"))
//                .filter(p -> p.toString().endsWith(".java"))
//                .map(p -> lines(p))
//                .forEach(System.out::println);
        ///home/22341970826/workspace-intelliJ/estudos/src/main/java/capitulo8
        Files.list(Paths.get("/home/22341970826/workspace-intelliJ/estudos/src/main/java/capitulo8"))
                .filter(p -> p.toString().endsWith(".java"))
                .flatMap(p -> lines(p))
                .forEach(System.out::println);

        Files.list(Paths.get("/home/22341970826/workspace-intelliJ/estudos/src/main/java/capitulo8"))
                .filter(p -> p.toString().endsWith(".java"))
                .flatMap(p -> lines(p))
                .flatMapToInt(s -> s.chars())
                .forEach(System.out::println);
    }

    static Stream<String> lines(Path p){
        try {
            return Files.lines(p);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
