package capitulo8;

import java.util.Random;
import java.util.function.IntSupplier;
import java.util.stream.IntStream;

public class capitulo88 {
    public static void main (String ... args){
        (new Random().ints()).limit(10).forEach(System.out::println);
//        IntStream.generate(new Fibonacci()).limit(10).forEach(System.out::println);
        IntStream.generate(new IntSupplier() {
            private int anterior = 0;
            private int proximo = 1;
            @Override
            public int getAsInt() {
                proximo = proximo + anterior;
                anterior = proximo - anterior;
                return anterior;
            }
        }).limit(10).forEach(System.out::println);

        int maiorQue100 = IntStream.generate(new Fibonacci()).filter(value -> value > 100).findFirst().getAsInt();
        System.out.println(maiorQue100);

        IntStream.iterate(0, operand -> operand + 1).limit(10).forEach(System.out::println);

    }

}
