package capitulo8;

import capitulo2.Usuario;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

public class capitulo86 {
    public static void main (String ... args){
        Usuario usuario0 = new Usuario(200, "Lucas");
        Usuario usuario2 = new Usuario(220, "Pedro");
        Usuario usuario3 = new Usuario(300, "Camila");
        Usuario usuario4 = new Usuario(150, "João");
        Usuario usuario5 = new Usuario(90, "Clara");
        Usuario usuario6 = new Usuario(40, "Moisés");
        Usuario usuario7 = new Usuario(250, "Orácio");
        Usuario usuario8 = new Usuario(170, "Nicolas");
        Usuario usuario9 = new Usuario(190, "José");
        Usuario usuario1 = new Usuario(180, "André");
        Usuario usuario10 = new Usuario(80, "Moreira");
        Usuario usuario11 = new Usuario(210, "Rodrigo");
        Usuario usuario12 = new Usuario(230, "Paulo");

        List<Usuario> usuarios = Arrays.asList(usuario0, usuario1, usuario2, usuario3, usuario4, usuario5, usuario6, usuario7, usuario8,
                usuario9, usuario10, usuario11, usuario12);

    //        for (Usuario u: usuarios.stream()
    //             ) {}

        Iterator<Usuario> iterator = usuarios.stream().iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next().getNome());
        }

        usuarios.stream().iterator().forEachRemaining(System.out::println);
        usuarios.stream().forEach(System.out::println);

        boolean hasModerator = usuarios.stream().anyMatch(Usuario::isModerador);
        System.out.println(hasModerator);

        boolean allModerator = usuarios.stream().allMatch(Usuario::isModerador);
        System.out.println(allModerator);

        boolean nomeModerator = usuarios.stream().noneMatch(Usuario::isModerador);
        System.out.println(nomeModerator);

        long numeroElementos = usuarios.stream().count();
        System.out.println(numeroElementos);

        long numeroElemntosMenos2 = usuarios.stream().skip(2).count();
        System.out.println(numeroElemntosMenos2);

        long limitando = usuarios.stream().limit(5).count();
        System.out.println(limitando);

        usuarios.stream().limit(5).forEach(System.out::println);
        System.out.println("pulando linha");
        usuarios.stream().sorted(Comparator.comparing(Usuario::getNome)).limit(5).forEach(System.out::println);


       long vazio =  Stream.empty().count();
       System.out.println(vazio);

       Stream<Usuario> usuarioStream = Stream.of(usuario0,usuario1,usuario2);
       usuarioStream.forEach(System.out::println);

    }
}
