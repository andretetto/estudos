package capitulo8;

import capitulo2.Usuario;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;

public class capitulo8 {

    public static void main ( String ... args){
        Usuario usuario0 = new Usuario(200, "Lucas");
        Usuario usuario2 = new Usuario(220, "Pedro");
        Usuario usuario3 = new Usuario(300, "Camila");
        Usuario usuario4 = new Usuario(150, "João");
        Usuario usuario5 = new Usuario(90, "Clara");
        Usuario usuario6 = new Usuario(40, "Moisés");
        Usuario usuario7 = new Usuario(250, "Orácio");
        Usuario usuario8 = new Usuario(170, "Nicolas");
        Usuario usuario9 = new Usuario(190, "José");
        Usuario usuario1 = new Usuario(180, "André");
        Usuario usuario10 = new Usuario(80, "Moreira");
        Usuario usuario11= new Usuario(210, "Rodrigo");
        Usuario usuario12= new Usuario(230, "Paulo");

        List<Usuario> usuarios = Arrays.asList(usuario0,usuario1,usuario2,usuario3,usuario4,usuario5,usuario6,usuario7,usuario8,
                usuario9,usuario10,usuario11,usuario12);

        //usuarios.stream().filter(usuario -> usuario.getPontos() > 100).sorted(comparing(Usuario::getNome)).collect(toList()).forEach(System.out::println);

//        System.out.println(
//                usuarios.stream().filter(usuario -> usuario.getPontos() > 100).findAny().get().getNome()
//        );

        //usuarios.stream().filter(usuario -> usuario.getPontos() > 100).peek(System.out::println);
        //usuarios.stream().filter(usuario -> usuario.getPontos() > 100).peek(System.out::println).findAny();
        //usuarios.stream().filter(usuario -> usuario.getPontos() > 100).sorted(comparing(Usuario::getNome)).peek(System.out::println).collect(Collectors.toList());
        usuarios.stream().sorted(comparing(Usuario::getNome)).peek(System.out::println).findAny();

    }


}
