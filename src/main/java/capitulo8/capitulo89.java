package capitulo8;

import capitulo2.Usuario;

import java.util.Arrays;
import java.util.List;

public class capitulo89 {
    public static void main(String ... args){
        Usuario usuario0 = new Usuario(200, "Lucas");
        Usuario usuario2 = new Usuario(220, "Pedro");
        Usuario usuario3 = new Usuario(300, "Camila");
        Usuario usuario4 = new Usuario(150, "João");
        Usuario usuario5 = new Usuario(90, "Clara");
        Usuario usuario6 = new Usuario(40, "Moisés");
        Usuario usuario7 = new Usuario(250, "Orácio");
        Usuario usuario8 = new Usuario(170, "Nicolas");
        Usuario usuario9 = new Usuario(190, "José");
        Usuario usuario1 = new Usuario(180, "André");
        Usuario usuario10 = new Usuario(80, "Moreira");
        Usuario usuario11= new Usuario(210, "Rodrigo");
        Usuario usuario12= new Usuario(230, "Paulo");

        Grupo englishSpeakers = new Grupo();
        englishSpeakers.add(usuario1);
        englishSpeakers.add(usuario2);

        Grupo spanishSpeakers = new Grupo();
        spanishSpeakers.add(usuario3);
        spanishSpeakers.add(usuario4);

        List<Grupo> grupos = Arrays.asList(englishSpeakers,spanishSpeakers);

        //grupos.stream().map(grupo -> grupo.getUsuarios().stream()).forEach(System.out::println);
        grupos.stream().flatMap(grupo -> grupo.getUsuarios().stream()).forEach(System.out::println);




    }
}
