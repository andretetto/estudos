package capitulo8;

import capitulo2.Usuario;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.function.IntSupplier;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class capitulo87 {
    public static void main (String ... args){
        Usuario usuario0 = new Usuario(200, "Lucas");
        Usuario usuario2 = new Usuario(220, "Pedro");
        Usuario usuario3 = new Usuario(300, "Camila");
        Usuario usuario4 = new Usuario(150, "João");
        Usuario usuario5 = new Usuario(90, "Clara");
        Usuario usuario6 = new Usuario(40, "Moisés");
        Usuario usuario7 = new Usuario(250, "Orácio");
        Usuario usuario8 = new Usuario(170, "Nicolas");
        Usuario usuario9 = new Usuario(190, "José");
        Usuario usuario1 = new Usuario(180, "André");
        Usuario usuario10 = new Usuario(80, "Moreira");
        Usuario usuario11 = new Usuario(210, "Rodrigo");
        Usuario usuario12 = new Usuario(230, "Paulo");

        List<Usuario> usuarios = Arrays.asList(usuario0, usuario1, usuario2, usuario3, usuario4, usuario5, usuario6, usuario7, usuario8,
                usuario9, usuario10, usuario11, usuario12);

//        Random random = new Random(0);
//        Supplier<Integer> suplier = () -> random.nextInt();
//        Stream<Integer> stream = Stream.generate(suplier);

//        Random random = new Random(0);
//        IntSupplier suplier = () -> random.nextInt();
//        IntStream stream = IntStream.generate(suplier);

//        Stream infinito
//        Random random = new Random(0);
//        IntStream stream = IntStream.generate(() -> random.nextInt());
//        int valor =stream.sum();
//        System.out.println(valor);

//        Random random = new Random(0);
//        IntStream stream = IntStream.generate(() -> random.nextInt());
//        for (int i: stream.limit(100).toArray()) {
//            System.out.println(i);
//        }

//        Random random = new Random(0);
//        IntStream stream = IntStream.generate(() -> random.nextInt());
//        for (Integer i: stream.limit(100).boxed().collect(Collectors.toList()) ) {
//            System.out.println(i);
//        }

        Random random = new Random(0);
        List<Integer> list = IntStream.generate(() -> random.nextInt()).limit(100).boxed().collect(Collectors.toList());
        for (Integer i: list ) {
            System.out.println(i);
        }

        for (Integer i: IntStream.generate(() -> 1).limit(100).boxed().collect(Collectors.toList())) {
            System.out.println(i);
        }

        IntStream.generate(new Fibonacci()).limit(10).forEachOrdered(System.out::println);

     }
}
