package capitulo11;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

public class Subscription {
    private BigDecimal monthlyFee;
    private Customer customer;
    private LocalDateTime begin;
    private Optional<LocalDateTime> end;

    public Subscription(BigDecimal monthlyFee, Customer customer, LocalDateTime begin) {
        this.monthlyFee = monthlyFee;
        this.customer = customer;
        this.begin = begin;
        this.end = Optional.empty();
    }

    public Subscription(BigDecimal monthlyFee, Customer customer, LocalDateTime begin, LocalDateTime end) {
        this.monthlyFee = monthlyFee;
        this.customer = customer;
        this.begin = begin;
        this.end = Optional.of(end);
    }

    public BigDecimal getMonthlyFee() {
        return monthlyFee;
    }

    public Customer getCustomer() {
        return customer;
    }

    public LocalDateTime getBegin() {
        return begin;
    }

    public Optional<LocalDateTime> getEnd() {
        return end;
    }

    public BigDecimal getTotalPaid(){
        return getMonthlyFee().multiply(new BigDecimal(
                ChronoUnit.MONTHS.between(getBegin(),getEnd().orElse(LocalDateTime.now()))
        ));
    }

}
