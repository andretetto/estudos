package capitulo11;

import java.math.BigDecimal;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Arrays.asList;

public class capitulo11 {
    public static void main (String ... args){
        Customer paulo = new Customer("Paulo Silveira");
        Customer rodrigo = new Customer("Rodrigo Turini");
        Customer guilherme = new Customer("Guilherme Silveira");
        Customer adriano = new Customer("Adriano Almeida");

        Product bach = new Product("Bach Completo", Paths.get("/music/bach.mp3"), new BigDecimal(100));
        Product poderosas = new Product("Poderosas Anita", Paths.get("/music/poderosas.mp3"), new BigDecimal(90));
        Product bandeira = new Product("Bandeira Brasil", Paths.get("/images/brasil.mp3"), new BigDecimal(54));
        Product beauty = new Product("Beleza Americana", Paths.get("beauty.mov"), new BigDecimal(150));
        Product vingadores = new Product("Os Vingadores", Paths.get("/movies/vingadores.mov"), new BigDecimal(200));
        Product amelie = new Product("Amelie Pouplain", Paths.get("/movies/amelie.mov"), new BigDecimal(100));

        LocalDateTime today = LocalDateTime.now();
        LocalDateTime yesterday = today.minusDays(1);
        LocalDateTime lastMonth = today.minusMonths(1);

        Payment payment1 = new Payment(asList(bach, poderosas),today, paulo);
        Payment payment2 = new Payment(asList(bach, bandeira, amelie),yesterday, rodrigo);
        Payment payment3 = new Payment(asList(beauty,vingadores, bach),today, adriano);
        Payment payment4 = new Payment(asList(bach, poderosas, amelie),lastMonth, guilherme);
        Payment payment5 = new Payment(asList(beauty, amelie),yesterday, paulo);

        List<Payment> payments = asList(payment1, payment2, payment3, payment4, payment5);

        payments.stream().sorted(Comparator.comparing(Payment::getDate)).forEach(System.out::println);

        //payment1.getProducts().stream().map(Product::getPrice).reduce(BigDecimal::add).ifPresent(System.out::println);
        BigDecimal total = payments.stream()
                .map(payment -> payment.getProducts().stream().map(Product::getPrice).reduce(BigDecimal.ZERO, BigDecimal::add))
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        System.out.println(total);

        BigDecimal totalFlat = payments.stream().flatMap(p -> p.getProducts().stream().map(Product::getPrice))
                                .reduce(BigDecimal.ZERO, BigDecimal::add);
        System.out.println(totalFlat);

        payments.stream().flatMap(payment -> payment.getProducts().stream());
        //payments.stream().map(Payment::getProducts).flatMap(products -> products.stream())
        //payments.stream().map(Payment::getProducts).flatMap(List::stream);

        Map<Product,Long> totalProducts = payments.stream().flatMap(payment -> payment.getProducts().stream())
                                            .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        System.out.println( totalProducts);

        totalProducts.entrySet().stream().forEach(System.out::println);
        totalProducts.forEach((product, aLong) -> System.out.println(String.format("%s :%s", product, aLong)));

        totalProducts.entrySet().stream().max(Comparator.comparing(Map.Entry::getValue)).ifPresent(System.out::println);

        payments.stream().flatMap(payment -> payment.getProducts().stream())
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet().stream().max(Comparator.comparing(Map.Entry::getValue))
                .ifPresent(System.out::println);

        System.out.println("-----------");
        payments.stream().flatMap(payment -> payment.getProducts().stream())
                .collect(Collectors.groupingBy(Function.identity(),
                         Collectors.reducing(BigDecimal.ZERO, Product::getPrice,BigDecimal::add)))
                .entrySet().stream().sorted(Comparator.comparing(Map.Entry::getValue))
                .forEach(System.out::println);


        payments.stream().flatMap(payment -> payment.getProducts().stream())
                .collect(Collectors.toMap(Function.identity(),Product::getPrice,BigDecimal::add));

        System.out.println("-----------");

        payments.stream().collect(Collectors.groupingBy(Payment::getCustomer))
                .entrySet().stream().forEach(System.out::println);


        System.out.println("-----------");
        payments.stream()
                .collect(Collectors.groupingBy(Payment::getCustomer, Collectors.mapping(o -> o.getProducts(), Collectors.toList())));


        Map<Customer, List<List<Product>>> produtosPorCliente = payments.stream().collect(Collectors.groupingBy(Payment::getCustomer, Collectors.mapping(Payment::getProducts, Collectors.toList())));
        Map<Customer, List<Product>> produtosPorCliente2 = produtosPorCliente.entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey,
                        e -> e.getValue().stream().flatMap(List::stream).collect(Collectors.toList())));

        System.out.println("-----------");
        produtosPorCliente2.entrySet().stream()
                .sorted(Comparator.comparing(e->e.getKey().getName())).forEach(System.out::println);



        System.out.println("-----------");
        payments.stream().collect(
                Collectors.groupingBy(Payment::getCustomer,
                        Collectors.reducing(BigDecimal.ZERO,
                                p -> p.getProducts().stream().map(Product::getPrice).reduce(BigDecimal.ZERO, BigDecimal::add)
                        ,BigDecimal::add)));


//        Map<Customer, BigDecimal> valorTotalporCliente =  payments.stream().collect(Collectors.groupingBy(Payment::getCustomer,
//                Collectors.reducing(BigDecimal.ZERO, p->p.getProducts().stream().map(Product::getPrice).reduce(BigDecimal.ZERO, BigDecimal::add),BigDecimal::add)))

        Function<Payment, BigDecimal> totalPagamento =
                payment -> payment.getProducts().stream().map(Product::getPrice).reduce(BigDecimal.ZERO, BigDecimal::add);


        Map<Customer, BigDecimal> valorTotalporCliente =  payments.stream().collect(Collectors.groupingBy(Payment::getCustomer,
                Collectors.reducing(BigDecimal.ZERO, totalPagamento ,BigDecimal::add)));


        valorTotalporCliente.entrySet().stream().sorted(Comparator.comparing(Map.Entry::getValue)).forEach(System.out::println);
        valorTotalporCliente.entrySet().stream().max(Comparator.comparing(Map.Entry::getValue)).ifPresent(System.out::println);


        //payments.stream().collect(Collectors.groupingBy(payment -> payment.getDate().toLocalDate()));
        payments.stream().collect(Collectors.groupingBy(p->YearMonth.from(p.getDate()))).entrySet().stream().forEach(System.out::println);

//        payments.stream().collect(Collectors.groupingBy(payment -> YearMonth.from(payment.getDate()),
//                Collectors.reducing(BigDecimal.ZERO, totalPagamento, BigDecimal::add)))

        payments.stream().collect(Collectors.groupingBy(payment -> YearMonth.from(payment.getDate()),
                Collectors.reducing(BigDecimal.ZERO, totalPagamento, BigDecimal::add))).entrySet().stream().forEach(System.out::println);


        BigDecimal monthlyFee = new BigDecimal("99.90");
        Subscription s1 = new Subscription(monthlyFee, paulo, yesterday.minusMonths(5));
        Subscription s2 = new Subscription(monthlyFee, rodrigo, yesterday.minusMonths(8), today.minusMonths(1));
        Subscription s3 = new Subscription(monthlyFee, adriano, yesterday.minusMonths(5), today.minusMonths(2));

        List<Subscription> subscriptions = Arrays.asList(s1,s2,s3);

        //long meses = ChronoUnit.MONTHS.between(s1.getBegin(), LocalDateTime.now());
        long meses = ChronoUnit.MONTHS.between(s1.getBegin(), s1.getEnd().orElse(LocalDateTime.now()));


        s1.getMonthlyFee().multiply(
                new BigDecimal(
                        ChronoUnit.MONTHS.between(s1.getBegin(), s1.getEnd().orElse(LocalDateTime.now())))
        );

        BigDecimal totalPago = subscriptions.stream().collect(Collectors.reducing(BigDecimal.ZERO, Subscription::getTotalPaid , BigDecimal::add));
        System.out.println(totalPago);

        BigDecimal totalPago1 = subscriptions.stream().map(Subscription::getTotalPaid).reduce(BigDecimal.ZERO, BigDecimal::add);
        System.out.println(totalPago1);


    }
}
