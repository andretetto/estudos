package capitulo7;

import capitulo2.Usuario;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

public class capitulo73 {
    public static void main (String ... args){
        Usuario usuario0 = new Usuario(200, "Lucas");
        Usuario usuario1 = new Usuario(180, "André");
        Usuario usuario2 = new Usuario(220, "Pedro");
        Usuario usuario3 = new Usuario(300, "Camila");
        Usuario usuario4 = new Usuario(150, "João");
        Usuario usuario5 = new Usuario(90, "Clara");
        Usuario usuario6 = new Usuario(40, "Moisés");
        Usuario usuario7 = new Usuario(250, "Orácio");
        Usuario usuario8 = new Usuario(170, "Nicolas");
        Usuario usuario9 = new Usuario(190, "José");
        Usuario usuario10 = new Usuario(80, "Moreira");
        Usuario usuario11 = new Usuario(210, "Rodrigo");
        Usuario usuario12 = new Usuario(230, "Paulo");

        List<Usuario> usuarios = Arrays.asList(usuario0,usuario1,usuario2,usuario3,usuario4,usuario5,usuario6,usuario7,usuario8,
                usuario9,usuario10,usuario11,usuario12);

//        List<Integer> pontos = new ArrayList<>();
//        usuarios.forEach(usuario -> pontos.add(usuario.getPontos()));

        List<Integer> pontos = usuarios.stream().map(Usuario::getPontos).collect(toList());

        IntStream intStream = usuarios.stream().mapToInt(Usuario::getPontos);
        double pontuacaoMedia = usuarios.stream().mapToInt(Usuario::getPontos).average().getAsDouble();
        System.out.println(pontuacaoMedia);


    }
}
