package capitulo7;

import capitulo2.Usuario;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toList;

public class capitulo72 {
    public static void main (String ... args) {
        Usuario usuario0 = new Usuario(200, "Lucas");
        Usuario usuario1 = new Usuario(180, "André");
        Usuario usuario2 = new Usuario(220, "Pedro");
        Usuario usuario3 = new Usuario(300, "Camila");
        Usuario usuario4 = new Usuario(150, "João");
        Usuario usuario5 = new Usuario(90, "Clara");
        Usuario usuario6 = new Usuario(40, "Moisés");
        Usuario usuario7 = new Usuario(250, "Orácio");
        Usuario usuario8 = new Usuario(170, "Nicolas");
        Usuario usuario9 = new Usuario(190, "José");
        Usuario usuario10 = new Usuario(80, "Moreira");
        Usuario usuario11 = new Usuario(210, "Rodrigo");
        Usuario usuario12 = new Usuario(230, "Paulo");

        List<Usuario> usuarios = Arrays.asList(usuario0,usuario1,usuario2,usuario3,usuario4,usuario5,usuario6,usuario7,usuario8,
                usuario9,usuario10,usuario11,usuario12);

//        List<Usuario> usuarios = new ArrayList<>();
//        usuarios.add(usuario0);
//        usuarios.add(usuario1);
//        usuarios.add(usuario2);
//        usuarios.add(usuario3);
//        usuarios.add(usuario4);
//        usuarios.add(usuario5);
//        usuarios.add(usuario6);
//        usuarios.add(usuario7);
//        usuarios.add(usuario8);
//        usuarios.add(usuario9);
//        usuarios.add(usuario10);
//        usuarios.add(usuario11);
//        usuarios.add(usuario12);

        //usuarios.sort(comparingInt(Usuario::getPontos).reversed());

//        List<Usuario> usuariosMaisDe100 = new ArrayList<>();
//        usuarios.forEach(usuario -> {
//            if (usuario.getPontos() > 100){
//                usuariosMaisDe100.add(usuario);
//            }
//        });
//
//        usuariosMaisDe100.forEach(Usuario::tornaModerador);
//        usuariosMaisDe100.forEach(System.out::println);

//        usuarios.removeIf(usuario -> usuario.getPontos() < 100);
//        usuarios.forEach(Usuario::tornaModerador);
//        usuarios.forEach(System.out::println);

//        Stream<Usuario> stream = usuarios.stream().filter(usuario -> usuario.getPontos() > 100);
//        stream.forEach(System.out::println);
        usuarios.stream().filter(usuario -> usuario.getPontos() > 100).forEach(Usuario::tornaModerador);
        usuarios.stream().filter(usuario -> usuario.getPontos() > 100).collect(toList()).forEach(System.out::println);
        //usuarios.stream().filter(usuario -> usuario.getPontos() > 100).collect(toCollection(HashSet::new)).forEach(System.out::println);
        //usuarios.stream().filter(usuario -> usuario.getPontos() > 100).toArray(usuario -> new Usuario[100]);
        usuarios.stream().filter(usuario -> usuario.getPontos() > 100).toArray(Usuario[]::new);

    }
}
