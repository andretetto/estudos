package capitulo7;

import capitulo2.Usuario;

import java.util.*;
import java.util.stream.IntStream;

import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.toList;

public class capitulo78 {
    public static void main (String ... args){
        Usuario usuario0 = new Usuario(200, "Lucas");
        Usuario usuario1 = new Usuario(180, "André");
        Usuario usuario2 = new Usuario(220, "Pedro");
        Usuario usuario3 = new Usuario(300, "Camila");
        Usuario usuario4 = new Usuario(150, "João");
        Usuario usuario5 = new Usuario(90, "Clara");
        Usuario usuario6 = new Usuario(40, "Moisés");
        Usuario usuario7 = new Usuario(250, "Orácio");
        Usuario usuario8 = new Usuario(170, "Nicolas");
        Usuario usuario9 = new Usuario(190, "José");
        Usuario usuario10 = new Usuario(80, "Moreira");
        Usuario usuario11 = new Usuario(210, "Rodrigo");
        Usuario usuario12 = new Usuario(230, "Paulo");

        List<Usuario> usuarios = Arrays.asList(usuario0,usuario1,usuario2,usuario3,usuario4,usuario5,usuario6,usuario7,usuario8,
                usuario9,usuario10,usuario11,usuario12);

//        List<Usuario> usuarios = Arrays.asList();

//         OptionalDouble optionalDouble = usuarios.stream().mapToInt(Usuario::getPontos).average();
//        double pontuacaoMedia =optionalDouble.orElse(0.0);

        double pontuacaoMedia = usuarios.stream().mapToInt(Usuario::getPontos).average().orElse(0.0);
        //double pontuacaoMedia = usuarios.stream().mapToInt(Usuario::getPontos).average().ifPresent();
        //double pontuacaoMedia = usuarios.stream().mapToInt(Usuario::getPontos).average().orElseThrow(IllegalAccessError::new);
        System.out.println(pontuacaoMedia);
        //Optional<String> stringOptional = usuarios.stream().max(comparingInt(Usuario::getPontos)).map(Usuario::getNome);
        //Optional<Usuario> max = usuarios.stream().max(comparingInt(Usuario::getPontos));
        String stringOptional = usuarios.stream().max(comparingInt(Usuario::getPontos)).map(Usuario::getNome).orElseThrow(IllegalAccessError::new);
        System.out.println(stringOptional);


    }
}
