package capitulo3;

public class Capitulo3 {
    public static void main(String ... args ) {
//        Runnable run = new Runnable() {
//            @Override
//            public void run() {
//                for (int i = 0; i < 1000; i++) {
//                    System.out.println(i);
//                }
//            }
//        };
//        new Thread(run).start();

//        System.out.println(run);
//        System.out.println(run.getClass());

        Runnable run = () -> {
            for (int i = 0; i < 1000; i++) {
                System.out.println(i);
            }
        };

        System.out.println(run);
        System.out.println(run.getClass());

//        new Thread(() -> {
//            for (int i = 0; i < 1000; i++) {
//                System.out.println(i);
//            }
//        }).start();

    }
}
