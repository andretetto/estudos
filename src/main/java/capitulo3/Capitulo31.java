package capitulo3;

public class Capitulo31 {
    public static void main (String ... args){
        Validador<String> validadorCEP = valor -> valor.matches("[0-9]{5}-[0-9]{3}");

        System.out.println(validadorCEP.validar("12349-123"));

    }
}
