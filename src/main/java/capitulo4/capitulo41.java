package capitulo4;

import capitulo2.Usuario;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class capitulo41 {
    public static void main(String ... args){
        Usuario usuario1 = new Usuario(100, "André");
        Usuario usuario2 = new Usuario(120, "Lucas");
        Usuario usuario3 = new Usuario(150, "Pedro");

        List<Usuario> usuarios = Arrays.asList(usuario1,usuario2,usuario3);

        Consumer<Usuario> monstraMensagem = u -> System.out.println("Antes de imprimir os nomes");
        Consumer<Usuario> imprimeNome = usuario -> System.out.println(usuario.getNome());

        usuarios.forEach(monstraMensagem.andThen(imprimeNome));
    }
}
