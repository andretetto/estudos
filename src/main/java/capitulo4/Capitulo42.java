package capitulo4;

import capitulo2.Usuario;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class Capitulo42 {
    public static void main (String ... args){
        Usuario usuario1 = new Usuario(150, "André");
        Usuario usuario2 = new Usuario(160, "Lucas");
        Usuario usuario3 = new Usuario(200, "Pedro");

        List<Usuario> usuarios = new ArrayList<>();
        usuarios.add(usuario1);
        usuarios.add(usuario2);
        usuarios.add(usuario3);

//        Predicate<Usuario> predicado = new Predicate<Usuario>() {
//            @Override
//            public boolean test(Usuario usuario) {
//                return usuario.getPontos() > 160;
//            }
//        };

        usuarios.removeIf(usuario -> usuario.getPontos() < 160);
        usuarios.forEach(usuario -> System.out.println(usuario.getNome()));
    }
}
