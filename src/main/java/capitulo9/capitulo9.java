package capitulo9;

import capitulo2.Usuario;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class capitulo9 {
        public static void main(String ... args) throws IOException {
            Stream<String> string = Files.list(Paths.get("/home/22341970826/workspace-intelliJ/estudos/src/main/java/capitulo8"))
                    .filter(p -> p.toString().endsWith(".java"))
                    .flatMap(p -> lines(p));

            LongStream lines = Files.list(Paths.get("/home/22341970826/workspace-intelliJ/estudos/src/main/java/capitulo8"))
                    .filter( p -> p.toString().endsWith(".java"))
                    .mapToLong( p -> lines(p).count());

            List<Long> linesLong = Files.list(Paths.get("/home/22341970826/workspace-intelliJ/estudos/src/main/java/capitulo8"))
                    .filter( p -> p.toString().endsWith(".java"))
                    .mapToLong( p -> lines(p).count())
                    .boxed().collect(Collectors.toList());

            List<Long> linesLong1 = Files.list(Paths.get("/home/22341970826/workspace-intelliJ/estudos/src/main/java/capitulo8"))
                    .filter(p -> p.toString().endsWith(".java"))
                    .map(p -> lines(p).count())
                    .collect(Collectors.toList());

            Map<Path, Long> linhasPorArquivo = new HashMap<>();
            Files.list(Paths.get("/home/22341970826/workspace-intelliJ/estudos/src/main/java/capitulo8"))
                    .filter(path -> path.toString().endsWith(".java"))
                    .forEach(path -> {
                        linhasPorArquivo.put(path, lines(path).count());
                    });
            System.out.println(linhasPorArquivo);
            linhasPorArquivo.clear();

            Files.list(Paths.get("/home/22341970826/workspace-intelliJ/estudos/src/main/java/capitulo8"))
                    .filter( path -> path.toString().endsWith(".java"))
                    .collect(Collectors.toMap(
                            p -> p,
                            p -> lines(p).count())
                    );
            System.out.println(linhasPorArquivo);

            System.out.println(linhasPorArquivo);
            linhasPorArquivo.clear();

            Files.list(Paths.get("/home/22341970826/workspace-intelliJ/estudos/src/main/java/capitulo8"))
                    .filter( path -> path.toString().endsWith(".java"))
                    .collect(Collectors.toMap(
                            Function.identity(),
                            p -> lines(p).count())
                    );
            System.out.println(linhasPorArquivo);


            Map<Path, List<String>> conteudoLinhasPorArquivo =
                    Files.list(Paths.get("/home/22341970826/workspace-intelliJ/estudos/src/main/java/capitulo8"))
                    .filter(path -> path.toString().endsWith(".java"))
                    .collect(Collectors.toMap(
                            Function.identity(),
                            path -> lines(path).collect(Collectors.toList())
                    ));
            System.out.println(conteudoLinhasPorArquivo);


            Usuario usuario0 = new Usuario(200, "Lucas");
            Usuario usuario2 = new Usuario(220, "Pedro");
            Usuario usuario3 = new Usuario(300, "Camila");
            Usuario usuario4 = new Usuario(150, "João");
            Usuario usuario5 = new Usuario(90, "Clara");
            Usuario usuario6 = new Usuario(40, "Moisés");
            Usuario usuario7 = new Usuario(250, "Orácio");
            Usuario usuario8 = new Usuario(170, "Nicolas");
            Usuario usuario9 = new Usuario(190, "José");
            Usuario usuario1 = new Usuario(180, "André");
            Usuario usuario10 = new Usuario(80, "Moreira");
            Usuario usuario11= new Usuario(210, "Rodrigo");
            Usuario usuario12= new Usuario(230, "Paulo");

            List<Usuario> usuarios = Arrays.asList(usuario0,usuario1,usuario2,usuario3,usuario4,usuario5,usuario6,usuario7,usuario8,
                    usuario9,usuario10,usuario11,usuario12);

            Map<String, Usuario> nomeParaUsuario = usuarios.stream().collect(Collectors.toMap(
                    Usuario::getNome,
                    Function.identity()
            ));

        }

        static Stream<String> lines (Path p){
            try {
                return Files.lines(p);
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
        }

}
