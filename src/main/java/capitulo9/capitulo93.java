package capitulo9;

import capitulo2.Usuario;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class capitulo93 {
    public static void main (String ... args){
        Usuario usuario0 = new Usuario(true, 200, "Lucas");
        Usuario usuario2 = new Usuario(true, 220, "Pedro");
        Usuario usuario3 = new Usuario(300, "Camila");
        Usuario usuario4 = new Usuario(300, "João");
        Usuario usuario5 = new Usuario(90, "Clara");
        Usuario usuario6 = new Usuario(40, "Moisés");
        Usuario usuario7 = new Usuario(250, "Orácio");
        Usuario usuario8 = new Usuario(170, "Nicolas");
        Usuario usuario9 = new Usuario(190, "José");
        Usuario usuario1 = new Usuario(190, "André");
        Usuario usuario10 = new Usuario(80, "Moreira");
        Usuario usuario11= new Usuario(210, "Rodrigo");
        Usuario usuario12= new Usuario(230, "Paulo");

        List<Usuario> usuarios = Arrays.asList(usuario0,usuario1,usuario2,usuario3,usuario4,usuario5,usuario6,usuario7,usuario8,
                usuario9,usuario10,usuario11,usuario12);

//        List<Usuario> usuariosFiltados = usuarios.stream().filter(usuario -> usuario.getPontos() > 100).sorted(Comparator.comparing(Usuario::getNome)).collect(Collectors.toList());
//        usuarios.stream().filter(usuario -> usuario.getPontos() > 100).sorted(Comparator.comparing(Usuario::getNome)).collect(Collectors.toList()).forEach(System.out::println);
//        usuarios.parallelStream().filter(usuario -> usuario.getPontos() > 100).sorted(Comparator.comparing(Usuario::getNome)).collect(Collectors.toList()).forEach(System.out::println);
//
//        Stream<Usuario> usuarioStream = usuarios.stream().sequential();

        //Long sum = LongStream.range(0,1_000_000_000).parallel().filter(x-> x % 2 == 0).sum();
//        Long sum = LongStream.range(0,1_000_000_000).filter(x-> x % 2 == 0).sum();
//        System.out.println(sum);

        List<Integer> listNumeros = Arrays.asList(1,2,3,4);
        listNumeros.stream().parallel().map(integer -> integer + 1).forEachOrdered(System.out::println);

        Set<Integer> setNumeros = new HashSet<>();
        setNumeros.add(1);
        setNumeros.add(2);
        setNumeros.add(3);
        setNumeros.add(4);
        setNumeros.stream().parallel().map(integer -> integer + 1).forEachOrdered(System.out::println);
    }
}
