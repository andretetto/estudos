package capitulo9;

import capitulo2.Usuario;

import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class capitulo92 {
    public static void main (String ... args){
        Usuario usuario0 = new Usuario(true, 200, "Lucas");
        Usuario usuario2 = new Usuario(true, 220, "Pedro");
        Usuario usuario3 = new Usuario(300, "Camila");
        Usuario usuario4 = new Usuario(300, "João");
        Usuario usuario5 = new Usuario(90, "Clara");
        Usuario usuario6 = new Usuario(40, "Moisés");
        Usuario usuario7 = new Usuario(250, "Orácio");
        Usuario usuario8 = new Usuario(170, "Nicolas");
        Usuario usuario9 = new Usuario(190, "José");
        Usuario usuario1 = new Usuario(190, "André");
        Usuario usuario10 = new Usuario(80, "Moreira");
        Usuario usuario11= new Usuario(210, "Rodrigo");
        Usuario usuario12= new Usuario(230, "Paulo");

        List<Usuario> usuarios = Arrays.asList(usuario0,usuario1,usuario2,usuario3,usuario4,usuario5,usuario6,usuario7,usuario8,
                usuario9,usuario10,usuario11,usuario12);


//        Map<Integer, List<Usuario>> pontuacao = new HashMap<>();
//        for (Usuario usuario: usuarios){
//            if (!pontuacao.containsKey(usuario.getPontos())){
//                pontuacao.put(usuario.getPontos(), new ArrayList<>());
//            }
//            pontuacao.get(usuario.getPontos()).add(usuario);
//        }

//        Map<Integer, List<Usuario>> pontuacao = new HashMap<>();
//        for (Usuario usuario: usuarios){
//            pontuacao.computeIfAbsent(usuario.getPontos(), u-> new ArrayList<>()).add(usuario);
//        }
        Map<Integer, List<Usuario>> pontuacao = usuarios.stream().collect(Collectors.groupingBy(Usuario:: getPontos));

        System.out.println(pontuacao);

        Map<Boolean, List<Usuario>> moderadores = usuarios.stream().collect(Collectors.partitioningBy(Usuario::isModerador));
        System.out.println(moderadores);

        Map<Boolean, List<String>> nomesPorTipo = usuarios.stream().collect(Collectors.partitioningBy(Usuario::isModerador,
                Collectors.mapping(Usuario::getNome, Collectors.toList())));

        System.out.println(nomesPorTipo);

        Map<Boolean, Integer> pontuacaoPorTipo = usuarios.stream().collect(Collectors.partitioningBy(Usuario::isModerador,
                Collectors.summingInt(Usuario::getPontos)));
        System.out.println(pontuacaoPorTipo);

        String nomes = usuarios.stream().map(Usuario::getNome).collect(Collectors.joining(", "));
        System.out.println(nomes);
    }
}
