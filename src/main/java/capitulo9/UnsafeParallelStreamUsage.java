package capitulo9;

import java.util.stream.LongStream;

public class UnsafeParallelStreamUsage {

    private static  long total = 0;

    public static void  main (String ... args){
        LongStream.range(1,1_000_000_000).parallel().filter(value -> value % 2 == 0).forEach(value -> total += value);
        System.out.println(total);
    }
}
